﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Descrição resumida para TesteTentarAcerto
    /// </summary>
    [TestClass]
    public class TesteTentarAcerto
    {
        public TesteTentarAcerto()
        {
            //
            // TODO: Adicionar lógica de construtor aqui
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtém ou define o contexto do teste que provê
        ///informação e funcionalidade da execução de teste atual.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Atributos de teste adicionais
        //
        // É possível usar os seguintes atributos adicionais enquanto escreve os testes:
        //
        // Use ClassInitialize para executar código antes de executar o primeiro teste na classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup para executar código após a execução de todos os testes em uma classe
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize para executar código antes de executar cada teste 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup para executar código após execução de cada teste
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Usadica()
        {
            int dicas_usadas = 0;
            dicas_usadas = dicas_usadas + 1;
            Assert.IsFalse(dicas_usadas > 10);
        }
        [TestMethod]
        public void AddPontos()
        {
            int jogador_pontos = 0;
            int moderador_pontos = 0;
            int dicas_usadas = 7;
            jogador_pontos = jogador_pontos + (10 - dicas_usadas);
            moderador_pontos = moderador_pontos + dicas_usadas;
            Assert.IsTrue(moderador_pontos == 7);
            Assert.IsTrue(jogador_pontos == 3);
        }
        [TestMethod]
        public void CheckVencedor()
        {
            int i = 0;
            int p = 0;
            int[] jogadores = new int[2];
            jogadores[0] = 40;
            jogadores[1] = 60;
            foreach (int j in jogadores)
            {
                if (jogadores[i] >= 50)
                {
                    p = 1;
                }
                else { i++; }
            }
            Assert.IsFalse(p > 1);
            Assert.IsFalse(p < 0);
        }
        [TestMethod]
        public void JogadorVez()
        {
            int vez = 0;
            int vez2 = 1;
            int vez3 = 2;
            int mediador = 2;
            int mediador2 = 3;
            int jogadorescount = 4;
            //Cenario 1
            vez = vez + 1;
            if (vez >= jogadorescount)
            {
                vez = 0;
            }
            if (vez == mediador)
            {
                vez = vez + 1;
            }
            if (vez >= jogadorescount)
            {
                vez = 0;
            }
            //Cenario 2
            vez2 = vez2 + 1;
            if (vez2 >= jogadorescount)
            {
                vez2 = 0;
            }
            if (vez2 == mediador)
            {
                vez2 = vez2 + 1;
            }
            if (vez >= jogadorescount)
            {
                vez2 = 0;
            }
            //Cenario 3
            vez3 = vez3 + 1;
            if (vez3 >= jogadorescount)
            {
                vez3 = 0;
            }
            if (vez3 == mediador2)
            {
                vez3 = vez3 + 1;
            }
            if (vez3 >= jogadorescount)
            {
                vez3 = 0;
            }
            Assert.IsTrue(vez == 1);
            Assert.IsTrue(vez2 == 3);
            Assert.IsTrue(vez3 == 0);
        }
    }
}
