﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using WpfApplicationPerfil.Modelo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Descrição resumida para TestePerfil
    /// </summary>
    [TestClass]
    public class TestePerfil
    {
        public TestePerfil()
        {
            //
            // TODO: Adicionar lógica de construtor aqui
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtém ou define o contexto do teste que provê
        ///informação e funcionalidade da execução de teste atual.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Atributos de teste adicionais
        //
        // É possível usar os seguintes atributos adicionais enquanto escreve os testes:
        //
        // Use ClassInitialize para executar código antes de executar o primeiro teste na classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup para executar código após a execução de todos os testes em uma classe
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize para executar código antes de executar cada teste 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup para executar código após execução de cada teste
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestIniciarPartida1()
        {
            List<String> Nomes = new List<String>();
            int qtd=0;
            Participante participante;
            List<Participante> participantes = new List<Participante>();
            int count = 0;
            List<int> ord = new List<int>();

            bool Check2=false, Check3=true, Check4=false;
            if (Check2)
            {
                qtd = 2;
                Nomes.Add("Matheus");
                Nomes.Add("Júlio");
            }
            if (Check3)
            {
                qtd = 3;
                Nomes.Add("Matheus");
                Nomes.Add("Júlio");
                Nomes.Add("Paulo");
            }
            if (Check4)
            {
                qtd = 4;
                Nomes.Add("Matheus");
                Nomes.Add("Júlio");
                Nomes.Add("Paulo");
            }
            /*-----------------------------------------------------*/
            Ordem ordem = new Ordem(qtd);
            ord = ordem.GetOrdem();
            foreach (String name in Nomes)
            {
                participante = new Participante(name, ord.ElementAt(count), 0, false, false);
                participantes.Add(participante);
                count += 1;
            }
        }

        [TestMethod]
        public void TestIniciarPartida()
        {
            Partida partida = new Partida();
            var WindowCadastro = new WindowCadastro();
            NCadastro Cad = new NCadastro();
            bool check1 = false, check2 = true, check3 = false;
            Cad.Cadastrar(check1, check2, check3);

        }
    }
}
