﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil
{
    public class Partida
    {
        private int QtdParticipantes;
        private int ValorDaVitoria = 50;
        private List<Participante> Participantes;

        public int Quantidade() { return QtdParticipantes; }
        public int ValorVitoria() { return ValorDaVitoria; }
        public List<Participante> Jogadores() { return Participantes; }

    }
}
