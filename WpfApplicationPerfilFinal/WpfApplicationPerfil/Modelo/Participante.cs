﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil
{
    public class Participante
    {
        private string Nome;
        private int Ordem;
        private int Placar;
        private bool JogadorDaVez;
        private bool MediadorDaVez;
        private bool Vencedor;

        public Participante(string Nome, int Ordem, int Placar, bool JogadorDaVez, bool Vencedor){
            this.Nome = Nome;
            this.Ordem = Ordem;
            this.Placar = Placar;
            this.JogadorDaVez = JogadorDaVez;
            this.Vencedor = Vencedor;
        }
        public override string ToString()
        {
            return Nome;
        }
        public void SetJogador(bool JogadorDaVez)
        {
            this.JogadorDaVez = JogadorDaVez;
        }
        public bool GetJogador()
        {
            return JogadorDaVez;
        }
        public void SetMediador(bool MediadorDaVez)
        {
            this.MediadorDaVez = MediadorDaVez;
        }
        public bool GetMediador()
        {
            return MediadorDaVez;
        }
        public int GetPlacar()
        {
            return Placar;
        }
        public void AddPlacar(int Pontos)
        {
            Placar = Placar + Pontos; 
        }
        public void SetVencedor()
        {
            Vencedor = true;
        }
        public bool GetVencedor()
        {
            return Vencedor;
        }

    }
}
