﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowCRUD.xaml
    /// </summary>
    public partial class WindowCRUD : Window
    {
        DataTable dt;
        public WindowCRUD(DataTable dt)
        {
            InitializeComponent();
            this.dt = dt;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string perfilCompleto;
            string nomeP;
            string categoriaP;
            string dicas;
            int indicePerfil;
            if (dica1.Text.ToString().Trim() == "" || dica2.Text.ToString().Trim() == "" || dica3.Text.ToString().Trim() == "" || dica4.Text.ToString().Trim() == "" || dica5.Text.ToString().Trim() == "" || dica6.Text.ToString().Trim() == "" || dica7.Text.ToString().Trim() == "" || dica8.Text.ToString().Trim() == "" || dica9.Text.ToString().Trim() == "" || dica10.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Preencha os dados corretamente!");
            }
            else
            {
                dicas = (dica1.Text.ToString().Trim() + ";" + dica2.Text.ToString().Trim() + ";" + dica3.Text.ToString().Trim() + ";" + dica4.Text.ToString().Trim() + ";" + dica5.Text.ToString().Trim() + ";" + dica6.Text.ToString().Trim() + ";" + dica7.Text.ToString().Trim() + ";" + dica8.Text.ToString().Trim() + ";" + dica9.Text.ToString().Trim() + ";" + dica10.Text.ToString().Trim());
                nomeP = nomePerfil.Text.ToString().Trim();
                categoriaP = categoriaPerfil.Text.ToString().Trim();

                perfilCompleto = nomeP + ";" + categoriaP + ";" + dicas;

                dt.Rows.Add(nomeP);
                NPerfil NPerf = new NPerfil();

                List<string> todosPerfis = NPerf.LePerfil();
                indicePerfil = NPerf.countPerfil();
                perfilCompleto = indicePerfil + ";" + perfilCompleto;

                NPerf.insertPerfil(perfilCompleto, todosPerfis, indicePerfil);
                MessageBox.Show("Criado com sucesso!");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (nomePerfilDel.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Informe o nome do perfil que deseja deletar!");
            }
            else
            {
                string nomePerfil = nomePerfilDel.Text.ToString().Trim();
                NPerfil NPerf = new NPerfil();
                List<string> Perfis = NPerf.LePerfil();
                if (NPerf.deletarPerfil(Perfis, nomePerfil))
                {
                    MessageBox.Show("Excluído com sucesso!");
                }
                else { MessageBox.Show("O perfil informado não existe!"); }
            }

        }
    }
}
