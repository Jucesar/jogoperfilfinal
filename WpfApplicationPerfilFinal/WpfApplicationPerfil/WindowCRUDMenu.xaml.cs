﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowCRUDMenu.xaml
    /// </summary>
    public partial class WindowCRUDMenu : Window
    {
        NPerfil Nperf = new NPerfil();
        DataTable dt = new DataTable();
        public WindowCRUDMenu()
        {
            InitializeComponent();
            dt.Columns.Add(new DataColumn("Nome"));
            Lista.ItemsSource = dt.DefaultView;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //List<string> perfis = Nperf.LePerfil();
            string[] perfil;
            foreach (string s in Nperf.LePerfil())
            {
                perfil = s.Split(';');
                if (perfil[0] != "")
                {
                    dt.Rows.Add(perfil[1]);
                }
                else return;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowCRUD Adicionar = new WindowCRUD(dt);
            Adicionar.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (nomePerfilDel.Text.ToString().Trim() == "")
            {
                MessageBox.Show("Informe o nome do perfil que deseja deletar!");
            }
            else
            {
                string nomePerfil = nomePerfilDel.Text.ToString().Trim();
                NPerfil NPerf = new NPerfil();
                List<string> Perfis = NPerf.LePerfil();
                if (NPerf.deletarPerfil(Perfis, nomePerfil))
                {
                    MessageBox.Show("Excluído com sucesso!");
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["Nome"].ToString() == nomePerfilDel.Text.ToString().Trim())
                            dr.Delete();
                    }
                    dt.AcceptChanges();
                }
                else { MessageBox.Show("O perfil informado não existe!"); }
            }
        }
    }
}
