﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowRodada.xaml
    /// </summary>
    public partial class WindowRodada : Window
    {
        NRodada Nrod;
        private List<Participante> jogadores;
        private int ordem;
        private int indice;

        public WindowRodada(List<Participante> jogadores, int ordem, int indice)
        {
            InitializeComponent();
            Nrod = new NRodada(jogadores, ordem);
            jogadorVez.Content = Nrod.JogadorVez();
            mediadorVez.Content = Nrod.nomeMediador();
            this.indice = indice;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Nrod.LePerfil();
            Dicas();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            if (Nrod.getDicasUsadas() >= 10)
            {
                MessageBox.Show("Mediador venceu!");
            }
        }


        void Dicas()
        {
            string[] perfil;
            foreach (string linha in Nrod.GetPerfil())
            {
                perfil = linha.Split(';');
                if (perfil[0] == indice.ToString())//indice determina qual perfil é lido, e é recebido do metodo NPartida.ComecaRodada, e cada rodada iniciada aumenta o indice em 1. Ver o construtor WindowRodada no topo
                {                                   //ah e perfil[0] é a primeira divisão da linha do perfil, que serve para indentificar. Se o indice é 1, o perfil mostrado será o que tem perfil[0]="1" (no caso, cerveja)
                    dica1.Content += perfil[3];
                    dica2.Content += perfil[4];
                    dica3.Content += perfil[5];
                    dica4.Content += perfil[6];
                    dica5.Content += perfil[7];
                    dica6.Content += perfil[8];
                    dica7.Content += perfil[9];
                    dica8.Content += perfil[10];
                    dica9.Content += perfil[11];
                    dica10.Content += perfil[12];
                    categoriaPerfil.Content += perfil[2];
                    perfilJogador.Content += perfil[1];
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Jogador da vez venceu!");
            Nrod.AddPontos();
            this.Close();
        }
        private void Button_Click_Dica_1(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica1.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_2(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica2.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_3(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica3.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_4(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica4.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_5(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica5.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_6(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica6.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_7(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica7.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_8(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica8.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_9(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica9.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Button_Click_Dica_10(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            Dica10.IsEnabled = false;
            if (Nrod.getDicasUsadas() >= 10)
            {
                Nrod.AddPontos();
                MessageBox.Show("Mediador venceu!");
                this.Close();
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (Nrod.CheckVencedor() == 1)
            {
                MessageBox.Show(Nrod.GetVencedor() + " é o vencedor!");
            }
        }
    }
}
