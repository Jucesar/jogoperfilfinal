﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowRodadaManager.xaml
    /// </summary>
    public partial class WindowRodadaManager : Window
    {
        NPartida Npart = new NPartida();

        public WindowRodadaManager(List<Participante> participantes, int quantidade)
        {
            Npart.Contagem(participantes, quantidade);

            InitializeComponent();
            Npart.Embaralha();
            partPartida.Text += Npart.Nomes();
            Npart.definirPapeis();
            //JogadorVez.Content += Npart.nomeJogador();
            //MediadorVez.Content += Npart.nomeMediador();           
        }

        private List<Participante> Ordenar()
        {
            return Npart.Embaralha();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Npart.CheckVencedor() == 1)
            {
                MessageBox.Show("Partida já acabou!");
                this.Close();
            }
            else
            {
                Npart.ComecaRodada();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Jogador1.Content = Npart.GetNome(0); Placar1.Content = Npart.GetPlacar(0).ToString();
            Jogador2.Content = Npart.GetNome(1); Placar2.Content = Npart.GetPlacar(1).ToString();
            if (Npart.GetQuantidade() > 2) { Jogador3.Content = Npart.GetNome(2); Placar3.Content = Npart.GetPlacar(2).ToString(); }
            if (Npart.GetQuantidade() > 3) { Jogador4.Content = Npart.GetNome(3); Placar4.Content = Npart.GetPlacar(3).ToString(); }
        }
        
    }
}
