﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplicationPerfil.Modelo;

namespace WpfApplicationPerfil.Negócio
{
    public class NCadastro
    {
        private int qtd;
        private Ordem ordem;
        private List<String> Nomes;
        private Participante participante;
        private List<Participante> participantes = new List<Participante>();
        private List<int> ord = new List<int>();
        private int count = 0;

        public void Cadastrar(bool Check2, bool Check3, bool Check4)
        {
            if (Check2)
            {
                qtd = 2;
                var CadastroNomes = new WindowCadastroNomes2();
                CadastroNomes.ShowDialog();
                Nomes = CadastroNomes.GetNomes();
            }
            if (Check3)
            {
                qtd = 3;
                var CadastroNomes = new WindowCadastroNomes3();
                CadastroNomes.ShowDialog();
                Nomes = CadastroNomes.GetNomes();
            }
            if (Check4)
            {
                qtd = 4;
                var CadastroNomes = new WindowCadastroNomes4();
                CadastroNomes.ShowDialog();
                Nomes = CadastroNomes.GetNomes();
            }

        }

        public void Repassar()
        {
            ordem = new Ordem(qtd);
            ord = ordem.GetOrdem();
            foreach (String name in Nomes)
            {
                participante = new Participante(name, ord.ElementAt(count), 0, false, false);
                participantes.Add(participante);
                count += 1;
            }
            var Partida = new WindowRodadaManager(participantes, qtd);
            Partida.ShowDialog();
        }

        public int Quantidade()
        {
            return qtd;
        }

        public List<String> NomeParticipantes()
        {
            return Nomes;
        }
    }
}
