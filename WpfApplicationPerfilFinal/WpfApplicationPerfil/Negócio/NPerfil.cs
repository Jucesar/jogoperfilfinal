﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Negócio
{
    public class NPerfil
    {

        private List<string> Perfis = new List<string>();

        public List<string> LePerfil()
        {
            string linha;
            StreamReader sr = new StreamReader("..\\..\\Negócio\\Perfis.txt");
            try
            {
                while (sr.Peek() > -1)
                {
                    linha = sr.ReadLine();
                    Perfis.Add(linha);
                }
            }
            finally
            {
                sr.Close();
            }
            return Perfis;
        }

        public void insertPerfil(string perfilNovo, List<string> todosPefis, int qtdPerfis)
        {
            int i = qtdPerfis - 1;
            using (StreamWriter sw = new StreamWriter("..\\..\\Negócio\\Perfis.txt"))
            {
                for (int j=0; j<=i; j++)
                {
                    sw.WriteLine(todosPefis[j]);
                }
                sw.WriteLine(perfilNovo);
            }
        }

        public void updatePerfil(List<string> todosPerfis)
        {
            using (StreamWriter sw = new StreamWriter("..\\..\\Negócio\\Perfis.txt"))
            {
                int j= todosPerfis.Count - 1;
                for (int i =0;i<=j;i++)
                {
                    sw.WriteLine(todosPerfis[i]);
                }
            }
        }

        public bool deletarPerfil(List<string> Perfis, string nomePerfil)
        {
            string[] perfil;
            int j = Perfis.Count;

            for (int i=0; i < j; i++)
            {
                perfil = Perfis[i].Split(';');
                if (perfil[1].ToString() == nomePerfil)
                {
                    Perfis.RemoveAt(i);
                    updatePerfil(Perfis);
                    return true;
                }
            }
            return false;
        }

        public int countPerfil()
        {
            int i = 0;
            foreach (string p in Perfis)
            {
                i++;
            }
            return i;
        }
    }
}
