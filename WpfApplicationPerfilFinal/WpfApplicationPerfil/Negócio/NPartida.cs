﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Negócio
{
    public class NPartida
    {
        private List<Participante> participantes;
        private string nomes;
        private int i;
        private int count = 0;
        private int indice = 0;

        public void Contagem(List<Participante> participantes, int quantidade)
        {
            this.participantes = participantes;
            int i = 0;
            while (i < quantidade)
            {
                if (i >= 1) this.nomes += ", ";
                this.nomes += participantes[i].ToString();
                i++;
            }
        }

        public List<Participante> Embaralha()
        {
            Random rand = new Random();
            foreach (Participante part in participantes)
            {
                i++;
            }
            for (int a = 0; a < 100; a++)
            {
                int c = rand.Next(i);
                int b = rand.Next(i);
                Participante part = participantes[c];
                participantes[c] = participantes[b];
                participantes[b] = part;

            }
            return participantes;
        }
        public void ComecaRodada()
        {
            var Rodada = new WindowRodada(participantes, count, indice);//Count = Ordem
            Rodada.ShowDialog();
            indice++;
            count++;//Count(ordem) aumenta em 1 a cada partida
            if (count >= participantes.Count)//se Count(ordem) ficar maior que o numero de jogadores, ele volta a ser 0, voltando para o primeiro moderador e recomeçando o loop
            {
                count = 0;
            }       
        }

        public string Nomes()
        {
            return nomes;
        }

        public void definirPapeis()
        {
            participantes [count].SetJogador(true);
            participantes[count + 1].SetMediador(true);
        }   

        public string nomeMediador()
        {
            return participantes[count].ToString();           
        }
        public string GetNome(int i)
        {
            return participantes[i].ToString();
        }
        public int GetPlacar(int i)
        {
            return participantes[i].GetPlacar();
        }
        public int GetQuantidade()
        {
            return participantes.Count;
        }
        public int CheckVencedor()
        {
            int i = 0;
            int p = 0;
            foreach (Participante part in participantes)
            {
                if (participantes[i].GetPlacar() >= 50)
                {
                    p = 1;
                }
                else { i++; }
            }
            return p;
        }
    }
}
