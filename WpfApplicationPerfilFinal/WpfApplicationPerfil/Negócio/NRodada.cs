﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Negócio
{
    public class NRodada
    {
          private List<Participante> jogadores;
          private int ordem;
          private int dicas_usadas = 0;
          private List<string> Perfis = new List<string>();
          private int vez = 0;
          private int mediador = 0;
         
        public NRodada(List<Participante> jogadores, int ordem)//Nota: esse "ordem" é o mesmo "count" de NPartida. Ver NPartida.ComecaRodada
        {
            this.jogadores = jogadores;
            this.ordem = ordem;
            mediador = ordem;
        }

        public string nomeMediador()
        {
            return "Mediador da vez: " + jogadores[mediador].ToString();//O problema era que "ordem" aumentava a cada rodada. Assim, ordem ficava maior que o numero de jogadores. Ver NPartida.ComecaRodada
        }

        public string JogadorVez()
        {
            vez = vez + 1;
            if (vez >= jogadores.Count)
            {
                vez = 0;
            }
            if (vez == mediador)
            {
                vez = vez + 1;
            }
            if (vez >= jogadores.Count)
            {
                vez = 0;
            }
            return "Jogador da vez: " + jogadores[vez].ToString();
        }
        public void AddPontos()
        {
            jogadores[vez].AddPlacar((10 - dicas_usadas));
            jogadores[mediador].AddPlacar((dicas_usadas));
        }
        public void LePerfil()
        {
            string linha;
            StreamReader sr = new StreamReader("..\\..\\Negócio\\Perfis.txt");
            try
            {
                while (sr.Peek() > -1)
                {
                    linha = sr.ReadLine();
                    Perfis.Add(linha);
                }
            }
            finally
            {
                sr.Close();
            }
        }
        public List<string> GetPerfil()
        {
            return Perfis;
        }

        public void usaDica()
        {
            dicas_usadas = dicas_usadas + 1;
        }
        public int getDicasUsadas()
        {
            return dicas_usadas;
        }
        public int CheckVencedor()
        {
            int i = 0;
            int p = 0;
            foreach (Participante part in jogadores)
            {
                if (jogadores[i].GetPlacar() >= 50)
                {
                    p = 1;
                }
                else { i++; }
            }
            return p;
        }
        public string GetVencedor()
        {
            int i = 0;
            int p = 0;
            foreach (Participante part in jogadores)
            {
                if (jogadores[i].GetPlacar() >= 50)
                {
                    p = i;
                }
                else { i++; }

            }
            return jogadores[p].ToString();
        }
    }
}
