﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Modelo;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Interaction logic for WindowCadastro.xaml
    /// </summary>
    public partial class WindowCadastro : Window
    {

        NCadastro Cad = new NCadastro();
        public WindowCadastro()
        {
            InitializeComponent();
        }

        public int Quantidade()
        {
            return Cad.Quantidade();
        }

        public List<String> NomeParticipantes()
        {
            return Cad.NomeParticipantes();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            Cad.Cadastrar(Rbt2.IsChecked.Value, Rbt3.IsChecked.Value, Rbt4.IsChecked.Value);
            this.Close();
            Cad.Repassar();
        }


    }
}
